import { v4 as uuid} from 'uuid';
import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
// import validator from '@middy/validator';

const dynamoDB = new AWS.DynamoDB.DocumentClient();
const eventBridge = new AWS.EventBridge();

async function createSession(event, context) {

    const { type, participants } = event.body;
    const { email } = event.requestContext.authorizer;

    const now = new Date();
    const endDate = new Date();
    endDate.setHours(now.getHours() + 1); // one hour in the future...make auction to run for 1 hour

    console.log(event);
    console.log(event.body);
    console.log("Adding a new session...");

    const session = {
        id : uuid(),
        appID: "no id",
        owner: email,
        participants,
        scheduled: now.toISOString(),
        start: now.toISOString(),
        end: now.toISOString(),
        status: "scheduled",
        type,
    };

    try {
      await dynamoDB.put({
        TableName: process.env.SESSIONS_TABLE_NAME,
        Item: session,
      }).promise();
    } catch (error) {
      console.error(error);
      throw new createError.InternalServerError(error);
    }

    const logs = await putEvent(session);
    console.log('sending the event' + logs);

    return {
      statusCode: 200,
      body: JSON.stringify( session ),
    };
}

function putEvent(session) {
  var params = {
    Entries: [
      {
        Detail: JSON.stringify(session),
        DetailType: 'SessionCreated',
        Source: 'createSession.event',
      },
    ]
  };

  return eventBridge.putEvents(params).promise();
}

export const handler = commonMiddleware(createSession);
  //.use(validator({ inputSchema: updateAuctionSchema }));