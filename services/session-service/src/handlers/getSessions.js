import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
//import validator from '@middy/validator';
// import getAuctionsSchema from '../lib/schemas/getAuctionsSchema';

const dynamoDB = new AWS.DynamoDB.DocumentClient();

async function getSessions(event, context) {
    let { status } = event.queryStringParameters;

    console.log(`retrieving Sessions for status = ${status}...`);

    let sessions;
    const params = {
        TableName: process.env.SESSIONS_TABLE_NAME,
        IndexName: 'statusAndEndDate',
        KeyConditionExpression: '#status = :status',
        ExpressionAttributeValues: {
            ':status': status,
        },
        ExpressionAttributeNames: {
            '#status': 'status',
        },
    };

    try {
        const result = await dynamoDB.query(params).promise();
        sessions = result.Items;
    } 
    catch (error) {
        console.error(error);
        throw new createError.InternalServerError(error);
    }
    
    return {
        statusCode: 200,
        body: JSON.stringify( sessions ),
    };
}

export const handler = commonMiddleware(getSessions);
    //.use(validator({ inputSchema: getAuctionsSchema, useDefaults: true}));


