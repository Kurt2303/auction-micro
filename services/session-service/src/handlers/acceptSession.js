//import { v4 as uuid} from 'uuid';
import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
import { getSessionByID } from './getSession';

// import validator from '@middy/validator';

const dynamoDB = new AWS.DynamoDB.DocumentClient();
//const eventBridge = new AWS.EventBridge();

async function acceptSession(event, context) {
    const  { id } = event.pathParameters;
    const { email } = event.requestContext.authorizer;

    console.log(event);
    console.log("Accepting a session...");

    console.log("Getting Session by ID...");
    const session = await getSessionByID(id);
    console.log(session);

    console.log("Getting participant from Session...");
    let participants = session.participants;

    if ( !participants ) {
       console.log(`Participants don't exist...`);
       throw new createError.NotFound(`Participants do not exist on the Session ${id}`); 
    }
    
    console.log(`Participants...`);
    console.log(participants);

    // get a user from participants
    let user = participants.filter(function (e) {
       return e.id === email;
    });

    console.log(`User...`);
    console.log(user);

    user[0].status = 'joined'; // update status

    console.log(`Updated User...`);
    console.log(user);

    let updatedParticipants = participants.filter(function (e) {
       return e.id !== email;
    });

    updatedParticipants = updatedParticipants.concat(user);

    const params = {
      TableName: process.env.SESSIONS_TABLE_NAME,
      Key: { id },
      UpdateExpression: 'set participants = :participants',
      ExpressionAttributeValues: {
          ':participants': updatedParticipants,
      },
      ReturnValues: 'ALL_NEW'
    };

    let updatedSession;
    try {
      const result = await dynamoDB.update(params).promise();
      updatedSession = result.Attributes;
    } catch (error) {
      console.error(error);
      throw new createError.InternalServerError(error);
    }

    // const logs = await putEvent(session);
    // console.log('sending the event' + logs);

    return {
      statusCode: 200,
      body: JSON.stringify( updatedSession ),
    };
}

// function putEvent(session) {
//   var params = {
//     Entries: [
//       {
//         Detail: JSON.stringify(session),
//         DetailType: 'Trigger Auction',
//         Source: 'auction.testing',
//       },
//     ]
//   };

//   return eventBridge.putEvents(params).promise();
// }

export const handler = commonMiddleware(acceptSession);
  //.use(validator({ inputSchema: updateAuctionSchema }));