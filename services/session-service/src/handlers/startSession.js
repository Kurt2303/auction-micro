//import { v4 as uuid} from 'uuid';
import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
import { getSessionByID } from './getSession';

// import validator from '@middy/validator';

const dynamoDB = new AWS.DynamoDB.DocumentClient();
//const eventBridge = new AWS.EventBridge();

async function acceptSession(event, context) {
    const  { id } = event.pathParameters;
    const { email } = event.requestContext.authorizer;
    const now = new Date();

    console.log(event);
    console.log("Starting a session...");

    console.log("Getting Session by ID...");
    const session = await getSessionByID(id);
    console.log(session);

    if (session.status !== 'scheduled') {
      throw new createError.Forbidden(`Error: Session ${session.id} is ${session.status}!. You can start only scheduled sessions.`);
    }
  
    if ( email !== session.owner){
      throw new createError.Forbidden(`Only owner can start a session. ${email} is not an owner.`); 
    }
  
    console.log(`Updating a Session...`);

    const params = {
      TableName: process.env.SESSIONS_TABLE_NAME,
      Key: { id },
      UpdateExpression: 'set #status = :status, #start = :startDate',
      ExpressionAttributeNames: {
        '#status' : 'status',
        '#start' : 'start',
      },
      ExpressionAttributeValues: {
          ':startDate': now.toISOString(),
          ':status': 'started',
      },
      ReturnValues: 'ALL_NEW'
    };

    let updatedSession;
    try {
      const result = await dynamoDB.update(params).promise();
      updatedSession = result.Attributes;
    } catch (error) {
      console.error(error);
      throw new createError.InternalServerError(error);
    }

    // const logs = await putEvent(session);
    // console.log('sending the event' + logs);

    return {
      statusCode: 200,
      body: JSON.stringify( updatedSession ),
    };
}

// function putEvent(session) {
//   var params = {
//     Entries: [
//       {
//         Detail: JSON.stringify(session),
//         DetailType: 'Trigger Auction',
//         Source: 'auction.testing',
//       },
//     ]
//   };

//   return eventBridge.putEvents(params).promise();
// }

export const handler = commonMiddleware(acceptSession);
  //.use(validator({ inputSchema: updateAuctionSchema }));