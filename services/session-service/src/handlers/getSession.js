import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
//import validator from '@middy/validator';
// import getAuctionsSchema from '../lib/schemas/getAuctionsSchema';

const dynamoDB = new AWS.DynamoDB.DocumentClient();

export async function getSessionByID(id) {
    let session;

    try {
        const result = await dynamoDB.get({
            TableName: process.env.SESSIONS_TABLE_NAME,
            Key: { id }
        }).promise();
    
        session = result.Item;
    } 
    catch (error) {
        console.error(error);
        throw new createError.InternalServerError(error);
    }

    if (!session) {
        throw new createError.NotFound(`Session with ID "${id}" not found`);
    }

    return session;
}

async function getSession(event, context) {
    const  { id } = event.pathParameters;
    const session = await getSessionByID(id);

    return {
        statusCode: 200,
        body: JSON.stringify( session ),
    };
}

export const handler = commonMiddleware(getSession);
    //.use(validator({ inputSchema: getAuctionsSchema, useDefaults: true}));


