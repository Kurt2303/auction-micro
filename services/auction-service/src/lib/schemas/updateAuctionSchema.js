const schema = {
    properties: {
        body: {
            type: 'object',
            properties: {
                title : {
                    type: 'string',
                },
                seller : {
                    type: 'string',
                },
                status : {
                    type: 'string',
                },
            },
        },
    },
    required: ['body'],
};

export default schema;