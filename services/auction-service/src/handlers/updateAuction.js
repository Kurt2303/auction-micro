import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
import validator from '@middy/validator';
import updateAuctionSchema from '../lib/schemas/updateAuctionSchema';

const dynamoDB = new AWS.DynamoDB.DocumentClient();

async function updateAuction(event, context) {
    const { id } = event.pathParameters;
    const { title, status, seller } = event.body;
    //const { email } = event.requestContext.authorizer;

    console.log(`status->${status} title->${title} seller->${seller}`);
  
    const params = {
        TableName: process.env.AUCTIONS_TABLE_NAME,
        Key: { id },
        UpdateExpression: 'set title = :title, seller = :seller, #status = :status',
        ExpressionAttributeNames: {
            '#status' : 'status',
        },
        ExpressionAttributeValues: {
            ':title': title,
            ':status': status,
            ':seller': seller,
        },
        ReturnValues: 'ALL_NEW'
    };
    
    let updatedAuction;

    try {
        const result = await dynamoDB.update(params).promise();
        updatedAuction = result.Attributes;
    } catch (error) {
        console.error(error);
        throw new createError.InternalServerError(error);
    }
    
    return {
        statusCode: 200,
        body: JSON.stringify( updatedAuction ),
    };
}

export const handler = commonMiddleware(updateAuction)
  .use(validator({ inputSchema: updateAuctionSchema }));
