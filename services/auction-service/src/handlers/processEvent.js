import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import { createNewAuction } from './createAuction';

const dynamoDB = new AWS.DynamoDB.DocumentClient();

async function processEvent(event, context) {
  console.log(`Event bus event arrived...`);
  console.log("Event:");
  console.log(event);
  console.log("Event source:");
  console.log(event.source);
  console.log("Event detail:");
  console.log(event.detail);

  const { detail } = event;

  const auction = await createNewAuction(event.source, detail.login);

  return {
    statusCode: 201,
    body: JSON.stringify(auction),
  };
}

export const handler = commonMiddleware(processEvent);


