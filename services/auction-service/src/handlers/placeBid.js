import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
import { getAuctionByID } from './getAuction';
import validator from '@middy/validator';
import placeBidSchema from '../lib/schemas/placeBidSchema';

const dynamoDB = new AWS.DynamoDB.DocumentClient();

async function placeBid(event, context) {
  const { id } = event.pathParameters;
  const { amount } = event.body;
  const { email } = event.requestContext.authorizer;

  const auction = await getAuctionByID(id);

  if (amount <= auction.highestBid.amount) {
      throw new createError.Forbidden(`Your bid must be greater than ${auction.highestBid.amount}!`);
  }

  if (auction.status !== 'OPEN') {
    throw new createError.Forbidden(`The auction ${auction.id} is already closed!`);
  }

  if ( email === auction.seller){
    throw new createError.Forbidden(`Seller ${email} can't bid on their own items!`); 
  }
  
  if ( email === auction.highestBid.bidder){
    throw new createError.Forbidden(`${email} is already a highest bidder!`); 
  }

  const params = {
    TableName: process.env.AUCTIONS_TABLE_NAME,
    Key: { id },
    UpdateExpression: 'set highestBid.amount = :amount, highestBid.bidder = :bidder',
    ExpressionAttributeValues: {
        ':amount': amount,
        ':bidder': email,
    },
    ReturnValues: 'ALL_NEW'
  };
  
  let updatedAuction;

  try {
    const result = await dynamoDB.update(params).promise();
    updatedAuction = result.Attributes;
  } catch (error) {
    console.error(error);
    throw new createError.InternalServerError(error);
  }
  
//   if (!auction) {
//     throw new createError.NotFound(`Auction with ID "${id}" not found`);
//   }

  return {
    statusCode: 200,
    body: JSON.stringify(updatedAuction),
  };
}

export const handler = commonMiddleware(placeBid)
  .use(validator({ inputSchema: placeBidSchema }));

