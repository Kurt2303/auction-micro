import AWS from 'aws-sdk';

const sns = new AWS.SNS()

export async function createSNSEndpoint(platformAppARN, aDeviceToken, userID){
    var params = {
        PlatformApplicationArn: platformAppARN, /* required */
        Token: aDeviceToken, /* required */
        Attributes: {
          'UserId': userID,
        },
    };

    try {
        console.log(`Creating SNS endpoint with params: ${JSON.stringify(params)}`);
        const result = await sns.createPlatformEndpoint(params, function(err, data){}).promise();
        return result;
    } 
    catch (error) {
        console.error(error);
        throw new createError.InternalServerError(error);
    }
}