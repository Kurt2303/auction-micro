import axios from 'axios';
// import AWS from 'aws-sdk';

export async function getAuth0Token(login){

    console.log('Getting Auth0 Token...');
    // Get secret Auth0 from AWS Secret Manager

    console.log(`region=> ${process.env.AWS_REGION_ENV}`);
    console.log(`sn=> ${process.env.SECRET_NAME}`);

    var AWS = require('aws-sdk'),
    region = process.env.AWS_REGION_ENV,
    secretName = process.env.SECRET_NAME,
    secret,
    decodedBinarySecret;

    var client = new AWS.SecretsManager({
        region: region
    });

    try {
        await client.getSecretValue({ SecretId: secretName }, function (err, data) {
            if (err) {
                console.log(err);
                //reject()
            }
            else {
                if ('SecretString' in data) {
                    decodedBinarySecret = JSON.parse(data.SecretString)[secretName];
                } else {
                    let buff = Buffer.from(data.SecretBinary, 'base64');
                    decodedBinarySecret = buff.toString('ascii');
                }
                //resolve()
            }
        }).promise();
    } 
    catch (error) {
        console.error(error);
        throw new createError.InternalServerError(error);
    }

    console.log('Decoded Secret:');
    console.log(decodedBinarySecret);

    try {
        let response = await axios.post(
            'https://sls-auction-eugene.us.auth0.com/oauth/token', 
            {
               grant_type: 'client_credentials',
               client_id: 'mudB5KvLTx4Cox9GPs3c4Uej6oLn7dSU',
               client_secret: decodedBinarySecret,
               audience: 'https://sls-auction-eugene.us.auth0.com/api/v2/'
            },
            {
                headers: { "content-type": "application/json" }
            }
        );
        console.log(response.data);

        const token = response.data.access_token;

        console.log(`Token =>`);
        console.log(token);

        try {
            console.log("Try create user...");
            let user = await axios.post(
                'https://sls-auction-eugene.us.auth0.com/api/v2/users', 
                {
                    connection: "Username-Password-Authentication",
                    email: login.email, 
                    password: "Welcome123$",
                    user_metadata: { 
                        "firstName" : login.firstName,
                        "lastName": login.lastName 
                    }, 
                    email_verified: false, 
                    verify_email: false, 
                    app_metadata: {} 
                },
                {
                    headers: { 
                        "content-type": "application/json",
                        "authorization": `Bearer ${token}`
                    }
                }
            );
            //console.log(response.data);
            const access_token = response.data.access_token;
            console.log(`Access Token: ${access_token}`);
            return access_token;
        } 
        catch (error) {
            console.error("User create fail....");
            console.error(error);
            return null;
        }
    }
    catch (error) {
        console.error(error);
        return null;
    }

    /**
     * 
     * Response Sample 
     * {
        "access_token": "eyJ...Ggg",
        "expires_in": 86400,
        "scope": "read:clients create:clients read:client_keys",
        "token_type": "Bearer"
    }
    * 
    */
}