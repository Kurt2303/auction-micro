import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
import {getUserByLogin} from './getUser';
//import validator from '@middy/validator';
// import getAuctionsSchema from '../lib/schemas/getAuctionsSchema';

const sns = new AWS.SNS()

async function onSessionCreate(event, context) {
    let aSession = event.detail;

    console.log(`onSessionCreate called...`);
    console.log(`session created:`);
    console.log(JSON.stringify( aSession ));
    let participants = aSession.participants;
    console.log(`Participants:`);
    console.log(JSON.stringify( participants ));

    if (!participants || participants.length < 1){
        throw new createError.NotFound();
    }

    const userPromises = participants.map(participant => getUserByLogin(participant.id));
    const users = await Promise.all(userPromises);

    console.log(`Users:`);
    console.log(users);

    const pushPromises = users.map(user => sendPushNotification(user, aSession));
    const pushes = await Promise.all(pushPromises);
    console.log(`Push sent`);
    console.log(pushes);

    return {
        statusCode: 200,
        body: JSON.stringify( {"Message":"Push sent on session create!"} ),
    };
}

export async function sendPushNotification(aUser, aSession) {
    if (!aUser.snsEndpoint){
        console.log(`Empty ARN...skip the push`);
        return {"Error":`No endpoint ARN for user ${aUser.login}`}
    }

    console.log(`Sending a push for -> ${aUser.snsEndpoint}`);
    var payload = {
        APNS_SANDBOX: {
            aps: {
                alert: {
                    title: `Hello ${aUser.login}!`,
                    body: `${aSession.owner} invites you to join a Calmo session!`
                },
                category: `SESSION_INVITE`,
                sound: `bingbong.aiff`,
                badge: 1
            },
            session:{
                id: aSession.id,
                status: aSession.status,
                scheduled: aSession.scheduled,
            }
        }
    };

    // first have to stringify the inner APNS object...
    payload.APNS_SANDBOX = JSON.stringify(payload.APNS_SANDBOX);
    // then have to stringify the entire message payload
    payload = JSON.stringify(payload);

    console.log(`sending push for user ${aUser.login}`);
    const params = {  Message: payload,      // Required
                      MessageStructure: 'json',
                      TargetArn: aUser.snsEndpoint, // Required}
                   }; 

    const result = await sns.publish(params).promise()

    return result;
}

export const handler = commonMiddleware(onSessionCreate);
    //.use(validator({ inputSchema: getAuctionsSchema, useDefaults: true}));


