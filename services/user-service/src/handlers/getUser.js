import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
//import validator from '@middy/validator';
// import getAuctionsSchema from '../lib/schemas/getAuctionsSchema';

const dynamoDB = new AWS.DynamoDB.DocumentClient();

export async function getUserByLogin(login) {
    let user;

    try {
        const params = {
            TableName: process.env.USERS_TABLE_NAME,
            IndexName: 'byLogin',
            KeyConditionExpression: 'login = :login',
            ExpressionAttributeValues: {
                ':login': login,
            },
        };
    
        const result = await dynamoDB.query(params).promise();
        user = result.Items;
    } 
    catch (error) {
        console.error(error);
        throw new createError.InternalServerError(error);
    }

    console.log(`User => ${JSON.stringify(user)}`);

    if (!user || user.length == 0) {
        throw new createError.NotFound(`User with login "${login}" not found`);
    }

    if (user.length > 1){
        throw new createError.NotFound(`Several users with the same login...BAD Juju!!!!`);
    }

    return user[0];
}

async function getUser(event, context) {
    const { email } = event.requestContext.authorizer;

    console.log(`Getting user for login => ${email}`);

    const user = await getUserByLogin(email);

    return {
        statusCode: 200,
        body: JSON.stringify( user ),
    };
}

export const handler = commonMiddleware(getUser);
    //.use(validator({ inputSchema: getAuctionsSchema, useDefaults: true}));


