import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';

const dynamoDB = new AWS.DynamoDB.DocumentClient();

async function updateUser(event, context) {
    const { id } = event.pathParameters;
    const { firstName, lastName } = event.body;

    // we can use this to validate update only by authorized users
    const { email } = event.requestContext.authorizer; 

    console.log(event.body);
    console.log(`Updating ${id} user...`);
    console.log(`First name=>${firstName}`);
    console.log(`Last name=>${lastName}`);
    console.log(`$email=>${email}`);

    // check for existence of the user?

    const params = {
        TableName: process.env.USERS_TABLE_NAME,
        Key: { id },
        UpdateExpression: 'set firstName = :firstName, lastName = :lastName',
        ExpressionAttributeValues: {
            ':firstName': firstName,
            ':lastName': lastName,
        },
        ReturnValues: 'ALL_NEW'
    };

    let updatedUser;

    try {
        const result = await dynamoDB.update(params).promise();
        updatedUser = result.Attributes;
    } catch (error) {
        console.error(error);
        throw new createError.InternalServerError(error);
    }
    
    return {
        statusCode: 200,
        body: JSON.stringify( updatedUser ),
    };
};

export const handler = commonMiddleware(updateUser);