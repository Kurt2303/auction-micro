import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
import {createSNSEndpoint} from '../lib/createSNSEndpoint';

const dynamoDB = new AWS.DynamoDB.DocumentClient();

async function updateDeviceToken(event, context) {
    const { id } = event.pathParameters;
    const { deviceToken } = event.body;

    // we can use this to validate update only by authorized users
    const { email } = event.requestContext.authorizer; 

    console.log(event.body);
    console.log(`Updating ${id} user's device token...`);
    console.log(`Device Token=>${deviceToken}`);
    console.log(`$email=>${email}`);

    // check for existence of the user?

    try {
        console.log(`Creating SNS endpoint...`);
        const platformARN = 'arn:aws:sns:us-west-2:166436014722:app/APNS_SANDBOX/calmo' // make this const?
        const endpoint = await createSNSEndpoint( platformARN, deviceToken, email);
        const endpointArn = endpoint.EndpointArn;

        console.log(`SNS: endpoint ARN -> ${endpointArn}`);
        
        const params = {
            TableName: process.env.USERS_TABLE_NAME,
            Key: { id },
            UpdateExpression: 'set deviceToken = :deviceToken, snsEndpoint = :snsEndpoint',
            ExpressionAttributeValues: {
                ':deviceToken': deviceToken,
                ':snsEndpoint': endpointArn,
            },
            ReturnValues: 'ALL_NEW'
        };
    
        let updatedUser;

        try {
            console.log(`Updating user with SNS endpoint ARN and device token`);
            const result = await dynamoDB.update(params).promise();
            updatedUser = result.Attributes;
        } 
        catch (error) {
            console.log(`ERROR: failed to update user in Dynamo...`);
            console.error(error);
            throw new createError.InternalServerError(error);
        }

        console.log(`Update success!`);
        return {
            statusCode: 200,
            body: JSON.stringify( updatedUser ),
        };
    } 
    catch (error) {
        console.log(`ERROR: failed to create SNS endpoint...`);
        console.error(error);
        throw new createError.InternalServerError(error);
    }
};

export const handler = commonMiddleware(updateDeviceToken);