import { v4 as uuid} from 'uuid';
import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
// import validator from '@middy/validator';

import {getAuth0Token} from '../lib/getAuth0Token';

const dynamoDB = new AWS.DynamoDB.DocumentClient();
const eventBridge = new AWS.EventBridge();

async function createUser(event, context) {
  
    const { firstName, lastName, email } = event.body;

    const user = {
        id : uuid(),
        firstName: firstName,
        lastName: lastName,
        login: email,
    };

    console.log(event);
    console.log(event.body);
    console.log("Adding a new user...");
    console.log(`First name=>${firstName}`);
    console.log(`Last name=>${lastName}`);
    console.log(`$email=>${email}`);
    console.log(user);

    if (!email){
      throw new createError.BadRequest(`Email is required. Email provided => "${email}".`);
    }

    try {
      const token = await getAuth0Token({
        "firstName":firstName,
        "lastName": lastName,
        "email": email,
      });

      if (!token) { // empty..null
        console.error(`Empty token...`);
      }
      else{
        console.log(`GOT TOKEN => creating a new user in Dynamo`);

        try {
          await dynamoDB.put({
            TableName: process.env.USERS_TABLE_NAME,
            Item: user,
          }).promise();
        } catch (error) {
          console.error(error);
          throw new createError.InternalServerError(error);
        }
    
        console.log('User added to Dynamo');
        //const logs = await putEvent(user);
        //console.log('sending the event' + logs);
        
        return {
          statusCode: 200,
          body: JSON.stringify({ user }),
        };

      }
    } 
    catch (error) {
        console.error(error);
        throw new createError.InternalServerError(error);    
    }
}

function putEvent(user) {
  var params = {
    Entries: [
      {
        Detail: JSON.stringify(user),
        DetailType: 'Trigger Auction',
        Source: 'auction.testing',
      },
    ]
  };

  return eventBridge.putEvents(params).promise();
}

export const handler = commonMiddleware(createUser);
  //.use(validator({ inputSchema: updateAuctionSchema }));